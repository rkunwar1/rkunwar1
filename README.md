### Tools, Technologies, and Languages


<img src="https://upload.wikimedia.org/wikipedia/commons/3/39/Kubernetes_logo_without_workmark.svg" alt="k8s" width="55"/>
<img src="https://git-scm.com/images/logos/logomark-orange@2x.png" alt="git" width="60"/>
<img src="https://i.pinimg.com/originals/28/ec/74/28ec7440a57536eebad2931517aa1cce.png" alt="terraform" width="60"/>
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDsJ64i2ifiCe8IWfLiD2PsROfK5fFc2wqRyv0xrT2ED3DbnRyYxO-KEApfDPcxslSUCk&usqp=CAU" alt="packer" width="60"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="ansible" width="50"/>

<br />
<hr />
<img src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg" alt="python" width="55"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg" alt="html" width="60"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg" alt="css" width="42"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/8/82/Gnu-bash-logo.svg" alt="bash" width="90"/>

